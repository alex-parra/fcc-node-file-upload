'use strict';

var express = require('express');
var cors = require('cors');
const formidable = require('formidable');

// require and use "multer"...

var app = express();

app.use(cors());
app.use('/public', express.static(process.cwd() + '/public'));

app.get('/', function (req, res) {

  res.sendFile(process.cwd() + '/views/index.html');

});


app.post('/api/fileanalyse', (req, res) => {

  new formidable.IncomingForm().parse(req, (err, fields, files) => {
    if (err) {
      console.error('Error', err)
      throw err
    }
    
    const { upfile } = files;
    console.log(upfile);
    if( upfile.size > 0 ) {
      return res.json({
        name: upfile.name,
        type: upfile.type,
        size: upfile.size,
      });
    }
    return res.status(400).json({error: 'No file uploaded'});
  });
  
});



app.listen(process.env.PORT || 3000, function () {
  console.log('Node.js listening ...');
});
